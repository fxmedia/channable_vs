// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Invitee.h"
#include "VRBANISM_0043/Websocket/ReplyLiveStructs.h"
#include "DownloadManager.generated.h"

UCLASS()
class VRBANISM_0043_API ADownloadManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADownloadManager();

	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Download)
	int32 MaxDownloads = 1;

	UPROPERTY(BlueprintReadOnly, Category = Download)
	int32 CurrentDownloads = 0;

	UPROPERTY(BlueprintReadOnly, Category = Download)
	TArray<AInvitee*> InviteeDownloadList;

	UPROPERTY(BlueprintReadOnly, Category = Download)
	float KeepSeconds = 0.0f;

	UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = Download)
	float DownloadEveryS = 0.1f;

	UFUNCTION() void StartDownload(AInvitee* invitee);
	UFUNCTION() void FinishDownload();
	UFUNCTION() void AddDownload(AInvitee* invitee);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
