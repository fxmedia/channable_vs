// Fill out your copyright notice in the Description page of Project Settings.


#include "SeatsManager.h"

// Sets default values
ASeatsManager::ASeatsManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create components
	RootSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootSceneComponent"));
	SetRootComponent(RootSceneComponent);
	RootComponent = RootSceneComponent;

	// Set default values
	MinimalRows = 3;
	SpawnHeightOffset = 35.0f;
	SpawnRotationOffset = 180.0f;
}

// Called when the game starts or when spawned
void ASeatsManager::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ASeatsManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FTransform ASeatsManager::GetEmptySeat_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("ASeatsManager::GetEmptySeat_Implementation Executing in C++"));
	
	FTransform EmptyTransfrom = FTransform();
	return EmptyTransfrom;
}

bool ASeatsManager::HasEmptySeat_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("ASeatsManager::HasEmptySeat_Implementation Executing in C++"));

	return false;
}